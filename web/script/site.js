$(document).ready(function(){
    openFolder('');
});

$( ".not_implemented" ).click(function() {
    alert( "Не реализовано" );
});

function deleteByPath(path, id) {
    if(confirm('Вы действительно хотите удалить?')) {
        $.ajax({
            url: "http://server.dev",
            type: "POST",
            data: {'deletePath': path},
            dataType: 'json',
            success: function (data) {
                var code = data['code'];
                if (code === '200')
                {
                    $('#' + id + '').remove();
                    showAlertSuccess('"' + data['data'] + '" успешно удален.');
                }
                else
                if (code === '401')
                {
                    showAlertDanger('При подключении к серверу возникла ошибка.');
                }
                else
                if (code === '404')
                {
                    showAlertDanger('При удалении возникла ошибка.');
                }
            }
        });
    }
}

function openFolder(path) {
    $("#main_table > tbody").remove();
    $.ajax({
        type: "POST",
        url: "http://server.dev",
        data: {'path': path},
        dataType: 'json',
        success: function (data) {
            var code = data['code'];
            var breadcrumbs = data['breadcrumbs'];
            fillBreadcrumbs(breadcrumbs);
            if (code === '200') {
                var folders = data['data']['folders'];
                var files = data['data']['files'];
                if ((folders.length == 0) && (files.length == 0))
                {
                    $("#main_table").css("display", "none");
                    showAlertInfo('Папка пуста.');
                }
                else
                {
                    $("#main_table").css("display", "table");
                    for (i = 0; i < folders.length; i++) {
                        $('#main_table').append('<tr id="' + folders[i].id.substr(3) + '"></tr>');
                        $('#main_table > tbody > tr:last').append('<td><input type="checkbox"></td>');
                        $('#main_table > tbody > tr:last').append(
                            '<td>' +
                                '<a style="cursor: pointer;" ' + 'onclick="openFolder(\'' + folders[i].path +
                                    '\')">' + folders[i].name + '</a>' +
                                addContextMenu(folders[i].path, folders[i].id.substr(3)) +
                            '</td>');
                        $('#main_table > tbody > tr:last').append('<td>Папка</td>');
                        $('#main_table > tbody > tr:last').append('<td>--</td>');
                    }
                    for (i = 0; i < files.length; i++) {
                        $('#main_table').append('<tr id="' + files[i].id.substr(3) + '"></tr>');
                        $('#main_table > tbody > tr:last').append('<td><input type="checkbox"></td>');
                        $('#main_table > tbody > tr:last').append(
                            '<td>' + files[i]['name'] +
                                addContextMenu(files[i].path, files[i].id.substr(3)) +
                            '</td>');
                        $('#main_table > tbody > tr:last').append('<td>' + files[i]['size'] + '</td>');
                        $('#main_table > tbody > tr:last').append('<td>' + files[i]['client_modified'] + '</td>');
                    }
                }
            }
            else
                if (code === '401')
                {
                    showAlertDanger('При подключении к серверу возникла ошибка.');
                }
                else
                    if (code === '404')
                    {
                        showAlertDanger('При получении данных возникла ошибка.');
                    }
        }
    });
}

function fillBreadcrumbs(breadcrumbs) {
    $('#breadcrumbs_ol').remove();
    var keys = Object.keys(breadcrumbs);
    var html = '';
    html += '<ol id="breadcrumbs_ol" class="breadcrumb">';
    if (keys.length === 1)
    {
        html += '<li class="breadcrumb-item active menu_item" aria-current="page" onclick="openFolder(\'' + breadcrumbs[keys[0]] +
            '\')">CloudBag</li>';
    }
    else
    {
        html += '<li class="breadcrumb-item menu_item" onclick="openFolder(\'' + breadcrumbs[keys[0]] + '\')"><a>' +
            'CloudBag</a></li>';
        for (var i = 1; i < (keys.length - 1); i++)
        {
            html += '<li class="breadcrumb-item menu_item" onclick="openFolder(\'' + breadcrumbs[keys[i]] + '\')"><a>' +
                keys[i] + '</a></li>';
        }
        html += '<li class="breadcrumb-item active menu_item" aria-current="page" onclick="openFolder(\'' +
            breadcrumbs[keys[keys.length - 1]] + '\')">' + keys[keys.length - 1] + '</li>';
        html += '</ol>';
    }

    $('#breadcrumbs').append(html);
}

function addContextMenu(path, id) {
    html =  '<div class="dropdiv">' +
                '<ul class="dropdown-menu menu_list" role="menu" aria-labelledby="dLabel">' +
                    '<li style="menu_item" onclick="deleteByPath(\'' + path + '\', \'' +
                        id + '\' )" >' +
                        '<a tabindex="-1" href="#">Удалить</a>' +
                    '</li>' +
                '</ul>' +
            '</div>';

    return html;
}

function showAlertInfo(text) {
    $('.alert').remove();
    $('.alert_div').append(
        '<div class="alert alert-info alert-dismissable">' +
            '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' + text + '</div>');
    $(".alert").fadeTo(5000, 0).slideUp(500, function() {
        $(this).remove();
    });
}

function showAlertSuccess(text) {
    $('.alert').remove();
    $('.alert_div').append(
        '<div class="alert alert-success alert-dismissable">' +
            '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' +
            '<strong>Успех!</strong> ' + text +
        '</div>');
    $(".alert").fadeTo(5000, 0).slideUp(500, function() {
        $(this).remove();
    });
}

function showAlertDanger(text) {
    $('.alert').remove();
    $('.alert_div').append(
        '<div class="alert alert-danger alert-dismissable">' +
            '<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' +
            '<strong>Ошибка! </strong> ' + text +
        '</div>');
    $(".alert").fadeTo(5000, 0).slideUp(500, function() {
        $(this).remove();
    });
}