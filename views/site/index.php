<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <ul class="nav nav-buttons nav-stacked" style="background: #e7e7e7; margin-top: 117px; margin-bottom: 25px">
                    <li class="not_implemented"><a>Открыть</a></li>
                    <li class="not_implemented"><a>Добавить комментарий</a></li>
                    <li class="not_implemented"><a>Переименовать</a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-9">
                <div class="alert_div" style="height: 60px"></div>
                <nav id="breadcrumbs" aria-label="breadcrumb" role="navigation"></nav>
                <table id="main_table" class="table table-bordered table-hover table-condensed myTable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Название</th>
                        <th>Размер</th>
                        <th>Последнее изменение</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>